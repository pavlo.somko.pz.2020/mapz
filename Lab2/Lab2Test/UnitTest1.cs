﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
namespace Lab2Test
{
    public class Soldier : IComparer
    {
        public string Name { get; set; }
        public double High { get; set; }
        public int Age { get; set; }
        public double Weight { get; set; }

        public int Compare(object x, object y)
        {
            var val1 = (Soldier)x;
            var val2 = (Soldier)y;
            var res = val1.Weight.CompareTo(val2.Weight);
            return res;
        }
    }
    [TestClass]
    public class UnitTest1
    {

        [TestMethod]
        public void TestListSort()
        {
            //arrange
            List<Soldier> azov = new List<Soldier> {
                new Soldier { Name = "Andrey", High=1.8, Age = 24, Weight = 82},
                new Soldier { Name = "Oleg", High=1.68, Age = 21, Weight = 65},
                new Soldier { Name = "Vova", High=1.9, Age = 28, Weight = 97 },
                new Soldier { Name = "Roman", High=1.79, Age = 30, Weight = 79} };
            var expected_arr = new double[] { azov[2].Weight, azov[0].Weight, azov[3].Weight, azov[1].Weight };
            azov.Sort();
            //act
            var physique_arr = azov.Select(x => x.High).ToArray<double>();
            //assert
            CollectionAssert.AreEqual(expected_arr, physique_arr);

        }
        [TestMethod]
        public void TestListSelect()
        {
            //arrange
            List<Soldier> azov = new List<Soldier> { 
                new Soldier { Name = "Andrey", High=1.8, Age = 24, Weight = 82}, 
                new Soldier { Name = "Oleg", High=1.68, Age = 21, Weight = 65}, 
                new Soldier { Name = "Vova", High=1.9, Age = 28, Weight = 97 },
                new Soldier { Name = "Roman", High=1.79, Age = 30, Weight = 79} };
            var expected_arr = new  double[]{    azov[0].High, azov[1].High, azov[2].High, azov[3].High };
            azov.Sort();
            //act
            var physique_arr = azov.Select(x => x.High).ToArray<double>();
            //assert
            CollectionAssert.AreEqual(expected_arr, physique_arr);           
   
        }
        [TestMethod]
        public void TestListWhere()
        {
            List<Soldier> azov = new List<Soldier> {
                new Soldier { Name = "Andrey", High = 1.8, Age = 24, Weight = 82 },
                new Soldier { Name = "Oleg", High = 1.68, Age = 21, Weight = 65 }, 
                new Soldier { Name = "Vova", High = 1.9, Age = 28, Weight = 97 },
                new Soldier { Name = "Roman", High = 1.79, Age = 30, Weight = 79 },
                new Soldier { Name = "Artem", High = 1.9, Age = 30, Weight = 102 }
            };
            var expected_soldier = new Soldier { Name = "Artem", High = 1.9, Age = 30, Weight = 102 };

            var soldier = azov.Where(x => x.High >= 1.9 && x.Weight > 100).First();

            Assert.ReferenceEquals(expected_soldier, soldier);
        }
        [TestMethod]
        public void TestDictionaryWhereAndSelect()
        {
            Dictionary<int,Soldier> azov = new Dictionary<int,Soldier> { 
                { 100, new Soldier { Name = "Andrey", High = 1.8, Age = 24, Weight = 82} },
                { 44, new Soldier { Name = "Oleg", High = 1.68, Age = 21, Weight = 65 } },
                { 31, new Soldier { Name = "Vova", High = 1.9, Age = 28, Weight = 97 } },
                { 78, new Soldier { Name = "Roman", High = 1.79, Age = 30, Weight = 79 } },
                { 91, new Soldier { Name = "Artem", High = 1.9, Age = 30, Weight = 102 }}
            };
            Soldier[] expected_soldiers = new Soldier[] {
                new Soldier { Name = "Andrey", High = 1.8, Age = 24, Weight = 82 },
                new Soldier { Name = "Roman", High = 1.79, Age = 30, Weight = 79 },
                new Soldier { Name = "Artem", High = 1.9, Age = 30, Weight = 102 }
            };

            var res = azov.Where(x => x.Key > 50).Select(x => x.Value).ToArray();

            CollectionAssert.ReferenceEquals(expected_soldiers, res);
        }
       
    }
}

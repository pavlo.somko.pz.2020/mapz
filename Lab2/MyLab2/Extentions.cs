﻿using System;
using System.Collections.Generic;
using System.Linq;

using static MyLab2.Program;

namespace MyLab2
{
    public static class ExtentionsClass
    {
        public static void HealSomeone(this Soldier soldier)
        {
            Console.WriteLine("Now I will help you");
        }
        public static void StudyCShurp(this Soldier soldier)
        {
            Random random = new Random();
            Console.WriteLine($"I will study C# {random.Next(1,10)}");
        }
    }
}

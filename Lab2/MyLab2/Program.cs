﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace MyLab2
{
    public class Program
    {
        void Test()
        {
            Soldier soldier = new Soldier { Name = "Name1", Age = 100, High = 1.6, Weight = 75 };

            var anonim = new
            {
                AnonimName = "Name2",
                AnonimAge = 90
            };
            Console.WriteLine($"Anonim name = {anonim.AnonimName}");
        }
        void Test2()
        {
            Soldier[] azov = new Soldier[] {
                new Soldier { Name = "Andrey", High=1.8, Age = 24, Weight = 82},
                new Soldier { Name = "Oleg", High=1.68, Age = 21, Weight = 65},
                new Soldier { Name = "Vova", High=1.9, Age = 28, Weight = 97 },
                new Soldier { Name = "Roman", High=1.79, Age = 30, Weight = 79} };
            var expected_arr = new double[] { azov[2].Weight, azov[0].Weight, azov[3].Weight, azov[1].Weight };
            Array.Sort(azov, new SoldierCompare());
        }
        static void Main(string[] args)
        {
            List<Soldier> azov = new List<Soldier> {
                new Soldier { Name = "Andrey", High=1.8, Age = 24, Weight = 82},
                new Soldier { Name = "Oleg", High=1.68, Age = 21, Weight = 65},
                new Soldier { Name = "Vova", High=1.9, Age = 28, Weight = 97 },
                new Soldier { Name = "Roman", High=1.79, Age = 30, Weight = 79} };
            azov.ForEach((x) => { Console.WriteLine($"{x.Name} {x.Age} {x.Weight} {x.High}"); });
            Console.WriteLine(new string('-',30));
            azov.Sort((x,y) => { return x.Age - y.Age; });
            azov.ForEach((x) => { Console.WriteLine($"{x.Name} {x.Age} {x.Weight} {x.High}"); });
            Console.WriteLine(new string('-', 30));
            azov = azov.OrderBy(x => x.Weight).ToList();
            azov.ForEach((x) => { Console.WriteLine($"{x.Name} {x.Age} {x.Weight} {x.High}"); });

        }
    }
    public class Soldier 
    {
        public string Name { get; set; }
        public double High { get; set; }
        public int Age { get; set; }
        public double Weight { get; set; }
      
    }
    public class SoldierCompare : IComparer<Soldier>
    {
        public int Compare(Soldier x, Soldier y)
        {
            return (int)(y.Weight - x.Weight);
        }
    }

}

﻿using System;

namespace Lab2
{
    public class MyLab
    {
        public class Soldier
        {
            public string Name { get; set; }
            public double High { get; set; }
            public int Age { get; set; }

            public double Weight { get; set; }
        }
    }
}

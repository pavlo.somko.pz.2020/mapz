
#include <iostream>
#include <string>
#include <chrono>
using namespace std;
using namespace chrono;

void StrTest() {
    string str;
    for (size_t i = 0; i < 10000000; i++)
    {
        str = *(new string(to_string(i)));
    }
}
void TestForArithmetic()
{
    double val1 = 1;
    float val2 = 2;
    int val3 = 3;
    for (int i = 0; i < 10000000; i++)
    {
        val1 = (i % 100000 + i % 2500) / (i % 100000 + 1);
        val2 = i % 1000 * i % 300;
        val3 = i - i % 10000;
    }
}
int main()
{
    auto start = steady_clock::now();
    TestForArithmetic();
    auto end = steady_clock::now();
    cout << "Elapsed time in milliseconds: "
        << duration_cast<milliseconds>(end - start).count()
        << " ms" << endl;
}

﻿using System;
using System.Diagnostics;

namespace lab1
{
    public class Program
    {
        Bayraktar bayraktar;
        IFlyable flyable = new Bayraktar();
        NULP uni = new NULP();
        Politeh my_uni;
        public void Test()
        {
            //var res1 = my_uni.ikni;
            //var res2 = my_uni.ikta;
            //res1 = uni.IKNI;
            //res2 = uni.IKTA;
        }
        public void Test2()
        {
            ClassOut classOut = new ClassOut();
            ClassOut.ClassIn val = new ClassOut.ClassIn();
            val.ValIn = 0;
            classOut.ValOut = 1;
            //ClassOut.ClassIn2 val2;

        }
        static public void Test3()
        {  
            var res1 = (Cities.Kharkiv & Cities.Kherson);// 0000 0011 & 0000 0100
            var res2 = (Weather.Sun ^ Weather.Rain); // 0000 0000 XOR  0000 0010
            
        }
        static public void Test4()
        {
            ImExPlicitTest imEx = new ImExPlicitTest { val = "Text" };
            string str = (string)imEx;
            str = "NewText";
            imEx = (ImExPlicitTest)str;

        }
        #region TaskPart2
        struct TestSt
        {
            public int val1 { get; set; }
            public double val2 { get; set; }
            public string val3 { get; set; }
        }
        class TestCl
        {
            public int val1 { get; set; }
            public double val2 { get; set; }
            public string val3 { get; set; }
        }
        class TestCl2 : TestCl
        {
            public float val4 { get; set; }
            public float val5 { get; set; }
        }
        static public void Test5()
        {
            TestCl val;
            for (int i = 0; i < 10000000; i++)
                val = new TestCl { val1 = i, val2 = i, val3 = "" };
        }
        static public void Test6()
        {
            TestSt val;
            for (int i = 0; i < 10000000; i++)
                val = new TestSt { val1 = i, val2 = i, val3 = "" };
        }
        static public void Test7()
        {
            TestCl2 val;
            for (int i = 0; i < 10000000; i++)
                val = new TestCl2 { val4 = i, val5 = i };
        }
        #endregion

        static void TestStr()
        {
            string str;
            for (int i = 0; i < 10000000; i++)
            {
                str = new string(i.ToString());
            }
        }
        static void TestForArithmetic()
        {
            double val1 = 1;
            float val2 = 2;
            int val3 = 3;
            for (int i = 0; i < 10000000; i++)
            {
                val1 = (i % 100000 + i%2500) / (i % 100000 + 1);
                val2 = i % 1000 * i % 300;
                val3 = i - i % 10000;
            }
        }
        static void Main(string[] args)
        {
            //Stopwatch stopwatch = new Stopwatch();
            //stopwatch.Start();
            //TestStr();
            //stopwatch.Stop();
            //Console.WriteLine(stopwatch.Elapsed);
            Test3();
        }
        #region Task1
        public interface IWeapon
        {
            public void KillMoskal();
        }
        public interface IFlyable : IWeapon
        {
            public void Fly();
        }
        public abstract class MilitaryEnginery : IWeapon
        {
            public MilitaryEnginery( string child)
            {
                Console.WriteLine("MilitaryEnginery + " + child);
            }
            public string name { get; set; }
            public string price { get; set; }

            public void KillMoskal() { }
        }
        public class BMP : MilitaryEnginery, IFlyable
        {
            public BMP():base("BMP")
            {
                Console.WriteLine("BMP");
            }
            public BMP(string name) : base(name)
            {
                Console.WriteLine(name);
            }

            public void Fly(){ }
        }
        public class Bayraktar : IFlyable, IWeapon
        {
            public void KillMoskal() { }
            public void Fly() { }
        }
        public  class Jawelina 
        {
            internal protected int val { get; set; }
        }
        #endregion

        #region Task2
        internal class InternalClass
        {

        }
        struct Politeh
        {
            public int ikni;
            int ikta;
        }
        class NULP
        {
            public int IKNI;
            int IKTA;
        }
        #endregion

        #region Task3
        public class ClassOut
        {
            public int ValOut { get; set; }
            internal class ClassIn
            {
                public int ValIn { get; set; }
            }
            class ClassIn2
            {

            }
        }
        #endregion

        #region Task4
        enum Cities
        {
            Kiev, Lviv, Kherson, Kharkiv, Odessa
        }
        [Flags]
        enum Weather
        {
            Sun=1, Rain=2, Snow=4, Wind=8, Fog=16
        }
        enum TestTheSame
        {
            One = 1, AlsoOne = 1, Two = 2
        }
        #endregion

    }
    #region Task5
    public class BaseOne
    {
        public virtual void Test() { }
    }
    public class ChildOne : BaseOne
    {
        public int val { get; set; }
        int[] numbers;
        public ChildOne(int val)
        {
            this.val = val;
            base.Test();
        }
        public int this[int index]
        {
            get { return numbers[index]; }
        }
    }
    public static class Extensions
    {
        public static void DoSmth(this ChildOne child) { }
    }
    #endregion

    #region Task6
    class ForStaticAndDynamic
    {
        int [] arrStatic = new int[5]; //статичне оголошення масиву
        int [] arrDynamic; // динамічне
        static int stat;
        public ForStaticAndDynamic()
        {
            arrDynamic = new int[5];
            Console.WriteLine( "dynamic " + DateTime.Now);
        }
        static ForStaticAndDynamic()
        { // викличеться лише 1 раз, при першому звернені до класа
            stat = 1;
            Console.WriteLine("static " + DateTime.Now);
        }
    }
    #endregion

    #region Task7
    class SecretData
    {
        public int Penta { get; set; }
        public string Gon { get; set; }
    }
    class ForRefAndOut
    {
        public void WithRefAndOutValueType(ref int val1, out int val2)
        {
            val1 = 5;
            val2 = 10;
        }
        public void WithoutRefAndOutRefType(SecretData val1)
        {
            val1 = new SecretData { Penta = 2, Gon = "Penta" };
        }
        public void WithRefAndOutRefType(ref SecretData val1, out SecretData val2)
        {
            val1 = new SecretData { Penta = 2, Gon = "Penta" };
            val2 = new SecretData { Penta = 3, Gon = "Gon" };
        }
        public void WithoutRef(SecretData val1)
        {
            val1.Penta = 11;
        }
        public void WithRef(ref SecretData val1)
        {
            val1.Penta = 12;
        }
        public void Test()
        {
            int val1 = 1, val2;
            WithRefAndOutValueType(ref val1, out val2);
            SecretData secret1 = new SecretData { Gon = "Gon", Penta = 1 }, secret2;
            WithoutRefAndOutRefType(secret1);

        }
    }
    #endregion

    #region Task8
    class BoxingTest
    {
        public void Test()
        {
            int val = 100;
            object boxing = val;
            int unboxing = (int)boxing;
        }
    }
    #endregion

    #region Task9
    class ImExPlicitTest
    {
        public string val { get; set; }
       
        public static implicit operator string(ImExPlicitTest obj)
        {
            return obj.val;
        }
        public static explicit operator ImExPlicitTest(string obj)
        {
            return new ImExPlicitTest { val = obj};
        }
    }
    #endregion

    #region Task10
    class OverrideObject : Object
    {
        public override bool Equals(object obj)
        {
            return obj is OverrideObject;
        }
        public override int GetHashCode()
        {
            return 0;
        }
        public override String ToString() 
        {
            return "OverrideObject";
        }
    }
    #endregion
}
